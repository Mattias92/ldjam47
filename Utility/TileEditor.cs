﻿using LDJam47.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47.Utility
{
    public class TileEditor
    {
        private readonly Game1 game;
        private List<Tile> tiles = new List<Tile>();
        private Texture2D textureAtlas;
        public Tile SelectedTile;
        public bool IntersectsEditor = false;

        private int maxTilesPerRow;
        private int neededRows;

        public TileEditor(Game1 game)
        {
            this.game = game;
            textureAtlas = game.Content.Load<Texture2D>("Atlas12");
            tiles.Add(new Tile(true, 0, 0));
            tiles.Add(new Tile(true, 1, 0));
            tiles.Add(new Tile(true, 2, 0));
            tiles.Add(new Tile(true, 3, 0));
            tiles.Add(new Tile(true, 4, 0));

            tiles.Add(new Tile(true, 0, 1));
            tiles.Add(new Tile(true, 1, 1));
            tiles.Add(new Tile(true, 2, 1));

            tiles.Add(new Tile(true, 1, 3));
            tiles.Add(new Tile(true, 2, 3));
            tiles.Add(new Tile(true, 3, 3));

            tiles.Add(new Tile(false, 3, 4));
            tiles.Add(new Tile(false, 4, 4));
            tiles.Add(new Tile(false, 0, 5));
            tiles.Add(new Tile(false, 1, 5));
            tiles.Add(new Tile(false, 2, 5));
            tiles.Add(new Tile(false, 3, 5));
            tiles.Add(new Tile(false, 4, 5, true));

            maxTilesPerRow = tiles.Count * Tile.Size - game.GraphicsDevice.Viewport.Width / Tile.Size;
            neededRows = (int)Math.Ceiling((double)tiles.Count / maxTilesPerRow);
            System.Diagnostics.Debug.WriteLine(neededRows);
        }

        public void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            int i = 0;
            IntersectsEditor = false;

            for (int y = 0; y < neededRows; y++)
            {
                int ys = (game.GraphicsDevice.Viewport.Height - neededRows * Tile.Size) + (y * Tile.Size);

                for (int x = 0; x < maxTilesPerRow && i < tiles.Count; x++, i++)
                {
                    int xs = x * Tile.Size;

                    if (mouseState.Y >= ys && mouseState.Y < ys + Tile.Size)
                    {
                        if (mouseState.X >= xs && mouseState.X < xs + Tile.Size)
                        {
                            IntersectsEditor = true;

                            if (mouseState.LeftButton == ButtonState.Pressed)
                            {
                                SelectedTile = tiles[i];
                            }

                            return;
                        }
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            int i = 0;
 
            for (int y = 0; y < neededRows; y++)
            {
                for (int x = 0; x < maxTilesPerRow && i < tiles.Count; x++, i++)
                {
                    Tile tile = tiles[i];
                    int bottom = game.GraphicsDevice.Viewport.Height - (y + 1) * Tile.Size;
                    spriteBatch.Draw(textureAtlas, new Vector2(x * Tile.Size, bottom), new Rectangle(tile.U * Tile.Size, tile.V * Tile.Size, Tile.Size, Tile.Size), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                }
            }
        }
    }
}
