﻿using LDJam47.World;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47
{
    public class ChangeChunkCommand : ICommand
    {
        private readonly Entity entity;
        private readonly Chunk prevChunk;
        private readonly Chunk nextChunk;
        private Vector2 next;
        private Vector2 prev;

        public ChangeChunkCommand(Entity entity, Chunk nextChunk, Vector2 next, Vector2 prev)
        {
            this.entity = entity;
            prevChunk = entity.LocalChunk;
            this.nextChunk = nextChunk;
            this.next = next;
            this.prev = prev;
        }

        public void Execute()
        {
            entity.LocalChunk.RemoveEntity(entity);
            entity.LocalChunk = nextChunk;
            entity.LocalChunk.AddEntity(entity);
            entity.Position = next;
            entity.Next = next;
        }

        public void Undo()
        {
            entity.LocalChunk.RemoveEntity(entity);
            entity.LocalChunk = prevChunk;
            entity.LocalChunk.AddEntity(entity);
            entity.Position = prev;
            entity.Next = prev;
        }
    }
}
