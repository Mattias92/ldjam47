﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47
{
    public interface ICommand
    {
        void Execute();
        void Undo();
    }
}
