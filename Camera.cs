﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47
{
    public class Camera
    {
        public readonly Viewport viewport;
        public Vector2 Position { get; set; }
        public Vector2 Origin { get; set; }
        public float Zoom { get; set; }

        private float rotation = 0f;

        public Camera(Viewport viewport)
        {
            this.viewport = viewport;
            Position = Vector2.Zero;
            Zoom = 1f;
            Origin = new Vector2(viewport.Width / 2, viewport.Height / 2) / Zoom;
        }

        public Matrix ViewMatrix()
        {
            Matrix transform = Matrix.Identity *
                    Matrix.CreateTranslation(-Position.X, -Position.Y, 0) *
                    Matrix.CreateRotationZ(rotation) *
                    Matrix.CreateTranslation(Origin.X, Origin.Y, 0) *
                    Matrix.CreateScale(new Vector3(Zoom, Zoom, Zoom));
            return transform;
        }

        public Vector2 ScreenToWorld(Vector2 position)
        {
            return Vector2.Transform(position - new Vector2(viewport.X, viewport.Y),
                Matrix.Invert(ViewMatrix()));
        }
    }
}
