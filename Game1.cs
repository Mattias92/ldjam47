﻿using LDJam47.Entities;
using LDJam47.Utility;
using LDJam47.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace LDJam47
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private List<Chunk> chunks = new List<Chunk>();
        public Chunk MainChunk;
        public bool IsEditing = false;
        private TileEditor tileEditor;
        private Camera camera;
        private GameConsole gameConsole;
        public CommandManager CommandManager;
        private KeyboardState previousKeyboardState = new KeyboardState();
        private string assemblyDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        public bool IsUndoing = false;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            //graphics.PreferredBackBufferWidth = 1280;
            //graphics.PreferredBackBufferHeight = 720;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Window.Title = "The Stone Maze by Sunblast";
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            camera = new Camera(GraphicsDevice.Viewport);
            gameConsole = new GameConsole(GraphicsDevice, Content);
            gameConsole.OnSave += Save;
            gameConsole.OnLoad += Load;
            gameConsole.OnCreate += Create;
            gameConsole.OnSetChunkNeighbor += SetChunkNeighbor;
            gameConsole.OnSetMainChunk += SetMainChunk;
            Window.TextInput += gameConsole.TextInputHandler;
            tileEditor = new TileEditor(this);

            Sfx.SoundEffects.Add("walking2", Content.Load<SoundEffect>("walking2"));
            Sfx.SoundEffects.Add("drop", Content.Load<SoundEffect>("drop"));
            Sfx.SoundEffects.Add("push", Content.Load<SoundEffect>("push2"));

            CommandManager = new CommandManager();
            Load(this, new EventArgs());
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        public void Load(object sender, EventArgs args)
        {
            string dir = Path.Combine(assemblyDirectory, Content.RootDirectory + "/world.bin");
            Stream openFileStream = File.OpenRead(dir);
            BinaryFormatter deserializer = new BinaryFormatter();
            SaveData saveData = (SaveData)deserializer.Deserialize(openFileStream);
            openFileStream.Close();
            chunks = saveData.Chunks;
            MainChunk = saveData.MainChunk;

            for (int i = 0; i < chunks.Count; i++)
            {
                chunks[i].Load(this, Content, camera);
            }
        }

        public void Create(object sender, ChunkArgs args)
        {
            Chunk chunk = new Chunk(this)
            {
                Name = args.Name
            };
            chunks.Add(chunk);
            MainChunk = chunk;
        }

        public void SetMainChunk(object sender, ChunkArgs chunkArgs)
        {
            for (int i = 0; i < chunks.Count; i++)
            {
                if (chunks[i].Name == chunkArgs.Name)
                {
                    MainChunk = chunks[i];
                }
            }
        }

        public void SetChunkNeighbor(object sender, ChunkArgs chunkArgs)
        {
            System.Diagnostics.Debug.WriteLine(chunkArgs.Name + chunkArgs.Neighbor);
            if (chunkArgs.Neighbor == "top")
            {
                if (TryGetChunk(chunkArgs.Name, out Chunk chunk))
                {
                    MainChunk.TopNeighbor = chunk;
                } 
            }
            else if (chunkArgs.Neighbor == "bottom")
            {
                if (TryGetChunk(chunkArgs.Name, out Chunk chunk))
                {
                    MainChunk.BottomNeighbor = chunk;
                }  
            }
            else if (chunkArgs.Neighbor == "left")
            {
                if (TryGetChunk(chunkArgs.Name, out Chunk chunk))
                {
                    MainChunk.LeftNeighbor = chunk;
                }
            }
            else if (chunkArgs.Neighbor == "right")
            {
                if (TryGetChunk(chunkArgs.Name, out Chunk chunk))
                {
                    MainChunk.RightNeighbor = chunk;
                }
            }
            else if (chunkArgs.Neighbor == "up")
            {
                if (TryGetChunk(chunkArgs.Name, out Chunk chunk))
                {
                    MainChunk.UpstairsNeighbor = chunk;
                }
            }
            else if (chunkArgs.Neighbor == "down")
            {
                if (TryGetChunk(chunkArgs.Name, out Chunk chunk))
                {
                    MainChunk.DownstairsNeighbor = chunk;
                }
            }
        }

        public bool TryGetChunk(string name, out Chunk chunk)
        {
            for (int i = 0; i < chunks.Count; i++)
            {
                if (chunks[i].Name == name)
                {
                    chunk = chunks[i];
                    return true;
                }
            }

            chunk = null;
            return false;
        }

        public void Save(object sender, EventArgs args)
        {
            string dir = Path.Combine(assemblyDirectory, Content.RootDirectory);
            SaveData saveData = new SaveData(chunks, MainChunk);
            Stream SaveFileStream = File.Create(Path.Combine(dir, "world.bin"));
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(SaveFileStream, saveData);
            SaveFileStream.Close();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();

            if (keyboardState.IsKeyDown(Keys.F5) && previousKeyboardState.IsKeyUp(Keys.F5) && IsEditing)
            {
                gameConsole.IsActive = !gameConsole.IsActive;
            }

            for (int i = 0; i < chunks.Count; i++)
            {
                chunks[i].PreUpdate(gameTime);
            }

            for (int i = 0; i < chunks.Count; i++)
            {
                chunks[i].Update(gameTime);
            }

            if (!IsUndoing)
            {
                for (int i = 0; i < chunks.Count; i++)
                {
                    chunks[i].PostUpdate(gameTime);
                }
            }

            camera.Origin = new Vector2(GraphicsDevice.Viewport.Width / 2 - ((Tile.Size * Chunk.Size) / 2), GraphicsDevice.Viewport.Height / 2 - ((Tile.Size * Chunk.Size) / 2));
            
            if (keyboardState.IsKeyDown(Keys.F1) && previousKeyboardState.IsKeyUp(Keys.F1) && IsEditing)
            {
                Vector2 screenToWorldPosition = camera.ScreenToWorld(mouseState.Position.ToVector2());

                int x = (int)screenToWorldPosition.X / Tile.Size * Tile.Size;
                int y = (int)screenToWorldPosition.Y / Tile.Size * Tile.Size;

                MainChunk.AddEntity(new Block(this, Content, new Vector2(x, y)));
            }

            tileEditor.Update(gameTime);

            if (!tileEditor.IntersectsEditor && IsEditing)
            {
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    Vector2 screenToWorldPosition = camera.ScreenToWorld(mouseState.Position.ToVector2());

                    if (MainChunk.Contains((int)screenToWorldPosition.X / 32, (int)screenToWorldPosition.Y / 32))
                    {
                        MainChunk.SetTile((int)screenToWorldPosition.X / 32, (int)screenToWorldPosition.Y / 32, tileEditor.SelectedTile);
                    }
                }

                if (mouseState.RightButton == ButtonState.Pressed)
                {
                    Vector2 screenToWorldPosition = camera.ScreenToWorld(mouseState.Position.ToVector2());

                    if (MainChunk.Contains((int)screenToWorldPosition.X / 32, (int)screenToWorldPosition.Y / 32))
                    {
                        MainChunk.SetTile((int)screenToWorldPosition.X / 32, (int)screenToWorldPosition.Y / 32, new Tile(true, 4, 3));
                    }
                }
            }

            base.Update(gameTime);
            previousKeyboardState = keyboardState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //GraphicsDevice.Clear(Color.CornflowerBlue);
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, camera.ViewMatrix());
            MainChunk.Draw(spriteBatch);
            spriteBatch.End();

            if (IsEditing)
            {
                spriteBatch.Begin();
                tileEditor.Draw(spriteBatch);
                spriteBatch.End();
            }

            gameConsole.Draw(spriteBatch);

            base.Draw(gameTime);
        }
    }
}
