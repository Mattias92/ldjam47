﻿using LDJam47.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47.World
{
    public class ChunkArgs : EventArgs
    {
        public string Name;
        public string Neighbor;
    }

    [Serializable()]
    public class Chunk
    {
        public string Name;
        public Chunk TopNeighbor;
        public Chunk RightNeighbor;
        public Chunk BottomNeighbor;
        public Chunk LeftNeighbor;
        public Chunk DownstairsNeighbor;
        public Chunk UpstairsNeighbor;

        public static int Size = 15;

        private List<Entity> entities = new List<Entity>();
        private Tile[][] tiles;
        [NonSerialized()]
        protected Texture2D texture;
        [NonSerialized()]
        private Game1 game;

        public Chunk(Game1 game)
        {
            this.game = game;
            texture = game.Content.Load<Texture2D>("atlas12");
            tiles = new Tile[Size][];

            for (int x = 0; x < Size; x++)
            {
                tiles[x] = new Tile[Size];

                for (int y = 0; y < Size; y++)
                {
                    tiles[x][y] = new Tile(false, 4, 5, true);
                }
            }
        }

        public bool TileHasCollision(int x, int y)
        {
            if (x >= 0 && x < Size && y >= 0 && y < Size)
            {
                return tiles[x][y].IsSolid;
            }
            return false;
        }

        public bool TileIsHole(int x, int y)
        {
            if (x >= 0 && x < Size && y >= 0 && y < Size)
            {
                return tiles[x][y].IsHole;
            }
            return false;
        }

        public bool Contains(int x, int y)
        {
            return (x >= 0 && x < Size && y >= 0 && y < Size);
        }

        public bool ContainsEntityWithCollision(Vector2 position)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i].Position == position)
                {
                    if (entities[i].HasCollision)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        public bool TryToMove(Vector2 next, Vector2 direction)
        {
            if (Contains((int)next.X / 32, (int)next.Y / 32))
            {
                if (TileHasCollision((int)next.X / 32, (int)next.Y / 32))
                {
                    return false;
                }
                else if (ContainsEntityWithCollision(next))
                {
                    return TryToMove(next + new Vector2(direction.X * 32, direction.Y * 32), direction);
                }
                else
                {
                    return true;
                }
            }
            else
            {
                if (direction.X == 1f && RightNeighbor != null)
                {
                    return RightNeighbor.TryToMove(new Vector2(0, next.Y), direction);
                }
                else if (direction.X == -1f && LeftNeighbor != null)
                {
                    return LeftNeighbor.TryToMove(new Vector2(Size * Tile.Size - Tile.Size, next.Y), direction);
                }
                if (direction.Y == -1f && TopNeighbor != null)
                {
                    return TopNeighbor.TryToMove(new Vector2(next.X, Size * Tile.Size - Tile.Size), direction);
                }
                else if (direction.Y == 1f && BottomNeighbor != null)
                {
                    return BottomNeighbor.TryToMove(new Vector2(next.X, 0), direction);
                }
            }

            return false;
        }

        public bool CheckForPushCase(Vector2 curr, Vector2 direction)
        {
            while (ContainsEntityWithCollision(curr += new Vector2(direction.X * Tile.Size, direction.Y * Tile.Size)))
            {
                if (ContainsEntityWithCollision(curr))
                {
                    if (UpstairsNeighbor != null && UpstairsNeighbor.TileIsHole((int)curr.X / Tile.Size, (int)curr.Y / Tile.Size) && UpstairsNeighbor.TryGetEntity(curr, out Entity entity))
                    {
                        ChangeChunkCommand changeChunkCommand = new ChangeChunkCommand(entity, this, entity.Position, entity.Position);
                        game.CommandManager.AddAndExecute(changeChunkCommand);

                        Move(curr, direction);
                        return true;
                    }
                }
            }

            return false;
        }

        public void Move(Vector2 curr, Vector2 direction)
        {
            Vector2 next = curr + new Vector2(direction.X * Tile.Size, direction.Y * Tile.Size);
            MoveCommand moveCommand = new MoveCommand(GetEntity(curr), next);
            game.CommandManager.AddAndExecute(moveCommand);

            if (Contains((int)next.X / Tile.Size, (int)next.Y / Tile.Size))
            {
                if (ContainsEntityWithCollision(next))
                {
                    Sfx.PlaySound("push", 0.15f);
                    Move(next, direction);
                }
            }
            else
            {
                if (direction.X == 1f && RightNeighbor != null)
                {
                    next = new Vector2(0, next.Y);
                    if (RightNeighbor.ContainsEntityWithCollision(next))
                    {
                        RightNeighbor.Move(next, direction);
                    }
                }
                else if (direction.X == -1f && LeftNeighbor != null)
                {
                    next = new Vector2(Size * Tile.Size - Tile.Size, next.Y);
                    if (LeftNeighbor.ContainsEntityWithCollision(next))
                    {
                        LeftNeighbor.Move(next, direction);
                    }
                }
                if (direction.Y == -1f && TopNeighbor != null)
                {
                    next = new Vector2(next.X, Size * Tile.Size - Tile.Size);
                    if (TopNeighbor.ContainsEntityWithCollision(next))
                    {
                        TopNeighbor.Move(next, direction);
                    }
                }
                else if (direction.Y == 1f && BottomNeighbor != null)
                {
                    next = new Vector2(next.X, 0);
                    if (BottomNeighbor.ContainsEntityWithCollision(next))
                    {
                        BottomNeighbor.Move(next, direction);
                    }
                }
            }
        }

        public void AddEntity(Entity entity)
        {
            entity.LocalChunk = this;
            entities.Add(entity);
        }

        public void RemoveEntity(Entity entity)
        {
            entities.Remove(entity);
        }

        public Entity GetEntity(Vector2 position)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i].Position == position)
                {
                    return entities[i];
                }
            }
            return null;
        }

        public bool TryGetEntity(Vector2 position, out Entity entity)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i].Position == position)
                {
                    entity = entities[i];
                    return true;
                }
            }

            entity = null;
            return false;
        }

        public void SetTile(int x, int y, Tile tile)
        {
            if (x >= 0 && x < Size && y >= 0 && y < Size)
            {
                Tile t = tiles[x][y];
                t.IsSolid = tile.IsSolid;
                t.IsHole = tile.IsHole;
                t.U = tile.U;
                t.V = tile.V;
            }
        }

        public Tile GetTile(int x, int y)
        {
            if (x >= 0 && x < Size && y >= 0 && y < Size)
            {
                return tiles[x][y];
            }
            return null;
        }

        public void Load(Game1 game, ContentManager contentManager, Camera camera)
        {
            this.game = game;
            texture = contentManager.Load<Texture2D>("atlas12");
            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Load(game, contentManager);

                if (entities[i] is Player player)
                {
                    player.Camera = camera;
                }
            }
        }

        public void PreUpdate(GameTime gameTime)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].PreUpdate(gameTime);
            }
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Update(gameTime);
            }
        }
    
        public void PostUpdate(GameTime gameTime)
        {
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    if (tiles[x][y].IsHole)
                    {
                        if (TryGetEntity(new Vector2(x * Tile.Size, y * Tile.Size), out Entity entity))
                        {
                            if (DownstairsNeighbor != null && !tiles[x][y].HoleIsBlocked)
                            {
                                ChangeChunkCommand changeChunkCommand = new ChangeChunkCommand(entity, DownstairsNeighbor, entity.Position, entity.Position);
                                game.CommandManager.AddAndExecute(changeChunkCommand);
                                Sfx.PlaySound("drop", 0.8f);
                            }
                        }

                        if (DownstairsNeighbor != null)
                        {
                            if (DownstairsNeighbor.TryGetEntity(new Vector2(x * Tile.Size, y * Tile.Size), out entity) && entity.HasCollision)
                            {
                                if (!tiles[x][y].HoleIsBlocked)
                                {
                                    TileSetCommand tileSetCommand = new TileSetCommand(tiles[x][y], 3, 4, true);
                                    game.CommandManager.AddAndExecute(tileSetCommand);
                                }
                            }
                            else
                            {
                                if (tiles[x][y].HoleIsBlocked)
                                {
                                    TileSetCommand tileSetCommand = new TileSetCommand(tiles[x][y], 4, 5, true);
                                    game.CommandManager.AddAndExecute(tileSetCommand);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    Tile tile = tiles[x][y];
                    spriteBatch.Draw(texture, new Vector2(x * Tile.Size, y * Tile.Size), new Rectangle(tile.U * Tile.Size, tile.V * Tile.Size, Tile.Size, Tile.Size), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                }
            }

            if (TopNeighbor != null)
            {
                TopNeighbor.Draw(spriteBatch, new Vector2(0f, Size * Tile.Size));
                
            }

            if (RightNeighbor != null)
            {
                RightNeighbor.Draw(spriteBatch, new Vector2(-(Size * Tile.Size), 0f));
            }

            if (BottomNeighbor != null)
            {
                BottomNeighbor.Draw(spriteBatch, new Vector2(0f, -(Size * Tile.Size)));
            }

            if (LeftNeighbor != null)
            {
                LeftNeighbor.Draw(spriteBatch, new Vector2(Size * Tile.Size, 0f));
            }

            for (int i = 0; i < entities.Count; i++)
            {
                Entity entity = entities[i];
                entity.Draw(spriteBatch);
            }
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 offset)
        {
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    Tile tile = tiles[x][y];
                    spriteBatch.Draw(texture, new Vector2(x * Tile.Size, y * Tile.Size), new Rectangle(tile.U * Tile.Size, tile.V * Tile.Size, Tile.Size, Tile.Size), Color.White, 0f, offset, 1f, SpriteEffects.None, 1f);
                }
            }

            for (int i = 0; i < entities.Count; i++)
            {
                Entity entity = entities[i];
                entity.Draw(spriteBatch, offset);
            }
        }
    }
}
