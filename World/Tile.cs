﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47.World
{
    [Serializable()]
    public class Tile
    {
        public bool IsSolid;
        public bool IsHole;
        public bool HoleIsBlocked = false;
        public int U;
        public int V;
        public static int Size = 32;

        public Tile(bool isSolid, int u, int v, bool isHole = false)
        {
            IsSolid = isSolid;
            U = u;
            V = v;
            IsHole = isHole;
        }
    }
}
