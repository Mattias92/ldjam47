﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47.World
{
    [Serializable()]
    public class SaveData
    {
        public List<Chunk> Chunks;
        public Chunk MainChunk;

        public SaveData(List<Chunk> chunks, Chunk mainChunk)
        {
            Chunks = chunks;
            MainChunk = mainChunk;
        }
    }
}
