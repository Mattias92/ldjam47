﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47
{
    public class CommandManager
    {
        private Stack<List<ICommand>> commands;
        private List<ICommand> commandsThisTurn;

        public CommandManager()
        {
            commands = new Stack<List<ICommand>>();
            commandsThisTurn = new List<ICommand>();
        }

        public void AddAndExecute(ICommand command)
        {
            command.Execute();
            commandsThisTurn.Add(command);
            System.Diagnostics.Debug.WriteLine(command.GetType());
        }

        public void Add(ICommand command)
        {
            commandsThisTurn.Add(command);
        }

        public void AddRange(IEnumerable<ICommand> commands)
        {
            commandsThisTurn.AddRange(commands);
        }

        public void Undo()
        {
            if (commandsThisTurn.Count > 0)
            {
                for (int i = 0; i < commandsThisTurn.Count; i++)
                {
                    commandsThisTurn[i].Undo();
                }

                commandsThisTurn.Clear();
            }
            else if (commands.Count > 0)
            {
                commandsThisTurn = commands.Pop();
                commandsThisTurn.Reverse();

                for (int i = 0; i < commandsThisTurn.Count; i++)
                {
                    commandsThisTurn[i].Undo();
                }

                commandsThisTurn.Clear();
            }
        }

        public void NewTurn()
        {
            commands.Push(commandsThisTurn);
            commandsThisTurn = new List<ICommand>();
        }

        public void Clear()
        {
            commands.Clear();
            commandsThisTurn.Clear();
        }

        public int Count
        {
            get
            {
                return commands.Count;
            }
        }
    }
}
