﻿using LDJam47.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47
{
    [Serializable()]
    public class Entity
    {
        protected float x;
        protected float y;
        public Vector2 Position
        {
            get
            {
                return new Vector2(x, y);
            }
            set
            {
                x = value.X;
                y = value.Y;
            }
        }
        [NonSerialized()]
        public Vector2 Next;
        public Chunk LocalChunk;
        public int LockedMovementFrames = 0;
        public static float MovementSpeed = 3f;
        public bool HasCollision = false;
        public bool ChangedState = false;

        [NonSerialized()]
        protected Texture2D texture;
        protected string texturePath;
        protected int u;
        protected int v;

        [NonSerialized()]
        public Game1 Game;

        public Entity(Game1 game)
        {
            Game = game;
        }

        public void ExecuteMove()
        {
            if (Position != Next)
            {
                if (Next.X > x)
                {
                    x += MovementSpeed;

                    if (Next.X <= Position.X)
                    {
                        x = Next.X;

                        if (!LocalChunk.Contains((int)x / 32, (int)y/ 32))
                        {
                            ChangeChunkCommand changeChunkCommand = new ChangeChunkCommand(this, LocalChunk.RightNeighbor, new Vector2(0, y), Position);
                            Game.CommandManager.AddAndExecute(changeChunkCommand);
                        }
                    }
                }
                else if (Next.X < x)
                {
                    x -= MovementSpeed;

                    if (Next.X >= x)
                    {
                        x = Next.X;

                        if (!LocalChunk.Contains((int)x / 32, (int)y / 32))
                        {
                            ChangeChunkCommand changeChunkCommand = new ChangeChunkCommand(this, LocalChunk.LeftNeighbor, new Vector2(Chunk.Size * Tile.Size - Tile.Size, y), Position);
                            Game.CommandManager.AddAndExecute(changeChunkCommand);
                        }
                    }
                }
                else if (Next.Y > y)
                {
                    y += MovementSpeed;

                    if (Next.Y <= y)
                    {
                        y = Next.Y;

                        if (!LocalChunk.Contains((int)x / 32, (int)y / 32))
                        {
                            ChangeChunkCommand changeChunkCommand = new ChangeChunkCommand(this, LocalChunk.BottomNeighbor, new Vector2(x, 0), Position);
                            Game.CommandManager.AddAndExecute(changeChunkCommand);
                        }
                    }
                }
                else if (Next.Y < y)
                {
                    y -= MovementSpeed;

                    if (Next.Y >= y)
                    {
                        y = Next.Y;

                        if (!LocalChunk.Contains((int)x / Tile.Size, (int)y / Tile.Size))
                        {
                            ChangeChunkCommand changeChunkCommand = new ChangeChunkCommand(this, LocalChunk.TopNeighbor, new Vector2(x, Chunk.Size * Tile.Size - Tile.Size), Position);
                            Game.CommandManager.AddAndExecute(changeChunkCommand);
                        }
                    }
                }

                ChangedState = true;
            }
        }

        public void Load(Game1 game, ContentManager contentManager)
        {
            Game = game;
            LockedMovementFrames = 0;
            texture = contentManager.Load<Texture2D>(texturePath);
            Next = Position;
        }

        public virtual void PreUpdate(GameTime gameTime)
        {

        }

        public virtual void Update(GameTime gameTime)
        {
            ChangedState = false;
        }

        public virtual void Draw(SpriteBatch spriteBatch) 
        {
            //spriteBatch.Draw(texture, Positon, new Rectangle(u * Tile.Size, v * Tile.Size, Tile.Size, Tile.Size), Color.White);
            spriteBatch.Draw(texture, Position, new Rectangle(u * Tile.Size, v * Tile.Size, Tile.Size, Tile.Size), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
        }

        public virtual void Draw(SpriteBatch spriteBatch, Vector2 offset)
        {
            spriteBatch.Draw(texture, Position, new Rectangle(u * Tile.Size, v * Tile.Size, Tile.Size, Tile.Size), Color.White, 0f, offset, 1f, SpriteEffects.None, 1f);
        }
    }
}
