﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47
{
    public class MoveCommand : ICommand
    {
        private readonly Entity entity;
        private Vector2 curr;
        private Vector2 next;

        public MoveCommand(Entity entity, Vector2 next)
        {
            this.entity = entity;
            curr = entity.Position;
            this.next = next;
        }

        public void Execute()
        {
            entity.Next = next;
        }

        public void Undo()
        {
            entity.Next = curr;
        }
    }
}
