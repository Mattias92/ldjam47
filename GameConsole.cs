﻿using LDJam47.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47
{
    public class GameConsole
    {
        public event EventHandler<ChunkArgs> OnCreate;
        public event EventHandler<EventArgs> OnLoad;
        public event EventHandler<EventArgs> OnSave;
        public event EventHandler<ChunkArgs> OnSetMainChunk;
        public event EventHandler<ChunkArgs> OnSetChunkNeighbor;

        public bool IsActive;

        private ContentManager contentManager;
        private GraphicsDevice graphicsDevice;
        private SpriteFont spriteFont;
        private Texture2D panelTexture;
        private RasterizerState panelRasterizerState;
        private int width;
        private int height;
        private float lineSpacing;
        private List<string> previousLines;
        private string line;

        public GameConsole(GraphicsDevice graphicsDevice, ContentManager contentManager)
        {
            IsActive = false;
            this.contentManager = contentManager;
            this.graphicsDevice = graphicsDevice;
            spriteFont = contentManager.Load<SpriteFont>("LatoRegular12");
            width = graphicsDevice.Viewport.Width;
            height = graphicsDevice.Viewport.Height / 3;
            lineSpacing = spriteFont.LineSpacing;
            panelTexture = new Texture2D(graphicsDevice, width, height);
            Color[] data = new Color[width * height];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = new Color(Color.Black, 0.5f);
            }
            panelTexture.SetData(data);
            panelRasterizerState = new RasterizerState
            {
                MultiSampleAntiAlias = true,
                ScissorTestEnable = true,
                FillMode = FillMode.Solid,
                CullMode = CullMode.CullCounterClockwiseFace,
                DepthBias = 0,
                SlopeScaleDepthBias = 0
            };
            previousLines = new List<string>();
            line = "";
        }

        public void TextInputHandler(object sender, TextInputEventArgs args)
        {
            if (!IsActive)
            {
                return;
            }

            DateTime nowDateTime = DateTime.Now;
            string currTime = nowDateTime.ToShortTimeString();

            char character = args.Character;
            Keys key = args.Key;

            if (key == Keys.Enter)
            {
                if (line.Length > 0)
                {
                    string[] parts = line.ToLower().Split(' ');

                    if (parts[0].Equals("save"))
                    {
                        OnSave?.Invoke(this, new EventArgs());
                        previousLines.Add("Saved!");
                        line = "";
                    }
                    else if (parts[0].Equals("go"))
                    {
                        if (parts.Length == 2)
                        {
                            ChunkArgs chunkArgs = new ChunkArgs
                            {
                                Name = parts[1]
                            };
                            OnSetMainChunk?.Invoke(this, chunkArgs);
                        }
                        line = "";
                    }
                    else if (parts[0].Equals("create"))
                    {
                        if (parts.Length == 2)
                        {
                            ChunkArgs chunkArgs = new ChunkArgs
                            {
                                Name = parts[1]
                            };
                            OnCreate?.Invoke(this, chunkArgs);
                        }
                        line = "";
                    }
                    else if (parts[0].Equals("set"))
                    {
                        if (parts.Length == 3)
                        {
                            ChunkArgs chunkArgs = new ChunkArgs
                            {
                                Neighbor = parts[1],
                                Name = parts[2]
                            };
                            OnSetChunkNeighbor?.Invoke(this, chunkArgs);
                        }
                        line = "";
                    }
                }
            }
            else if (key == Keys.Back)
            {
                if (line.Length > 0)
                {
                    line = line.Remove(line.Length - 1);
                }
            }
            else if (spriteFont.Characters.Contains(character))
            {
                line += character;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!IsActive)
            {
                return;
            }

            graphicsDevice.ScissorRectangle = new Rectangle(0, 0, width, height);

            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, panelRasterizerState, null, null);
            spriteBatch.Draw(panelTexture, Vector2.Zero, Color.White);

            string linePrompt = ">";
            float lineWidth = spriteFont.MeasureString(linePrompt).X + spriteFont.MeasureString(line).X;
            float lineOverflow = 0f;

            if (width - lineWidth < 0)
            {
                lineOverflow = width - lineWidth;
            }

            spriteBatch.DrawString(spriteFont, linePrompt + line, new Vector2(lineOverflow, height - lineSpacing), Color.White);

            for (int i = 0; i < previousLines.Count; i++)
            {
                spriteBatch.DrawString(spriteFont, previousLines[i], new Vector2(0, height - lineSpacing - (previousLines.Count * lineSpacing) + i * lineSpacing), Color.White);
            }

            spriteBatch.End();
        }
    }
}
