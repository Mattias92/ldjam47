﻿using LDJam47.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47.Entities
{
    [Serializable()]
    public class Player : Entity
    {
        [NonSerialized()]
        private KeyboardState previousKeyboardState = new KeyboardState();
        [NonSerialized()]
        public Camera Camera;
        public bool endingTurn = false;

        private int animationCount = 0;
        private int walkSoundCooldown = 0;

        public Player(Game1 game, ContentManager content, string texturePath, Vector2 position, Camera camera) : base(game)
        {
            this.texturePath = "player";
            texture = content.Load<Texture2D>("player");
            Position = position;
            Next = position;
            u = 0;
            v = 2;
            Camera = camera;
        }

        public override void PreUpdate(GameTime gameTime)
        {
            if (endingTurn)
            {
                if (!Game.IsUndoing)
                {
                    Game.CommandManager.NewTurn();
                }
                else
                {
                    Game.IsUndoing = false;
                }

                endingTurn = false;
            }
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();

            if (LockedMovementFrames == 0)
            {
                if (keyboardState.IsKeyDown(Keys.Up))
                {
                    if (LocalChunk.TryToMove(Position + new Vector2(0, -32), new Vector2(0, -1)))
                    {
                        if (LocalChunk.CheckForPushCase(Position, new Vector2(0, -1)))
                        {
                            Sfx.PlaySound("drop", 0.8f);
                        }
                        else
                        {
                            LocalChunk.Move(Position, new Vector2(0, -1));

                            if (walkSoundCooldown == 0)
                            {
                                Sfx.PlaySound("walking2", 0.4f);
                                walkSoundCooldown = 12;
                            }
                        }

                        LockedMovementFrames = (int)Math.Ceiling(32f / MovementSpeed) + 1;
                    }  
                }
                else if (keyboardState.IsKeyDown(Keys.Right))
                {
                    if (LocalChunk.TryToMove(Position + new Vector2(32, 0), new Vector2(1, 0)))
                    {
                        if (LocalChunk.CheckForPushCase(Position, new Vector2(1, 0)))
                        {
                            Sfx.PlaySound("drop", 0.8f);
                        }
                        else
                        {
                            LocalChunk.Move(Position, new Vector2(1, 0));

                            if (walkSoundCooldown == 0)
                            {
                                Sfx.PlaySound("walking2", 0.4f);
                                walkSoundCooldown = 12;
                            }
                        }

                        LockedMovementFrames = (int)Math.Ceiling(32f / MovementSpeed) + 1;
                    }
                }
                else if (keyboardState.IsKeyDown(Keys.Down))
                {
                    if (LocalChunk.TryToMove(Position + new Vector2(0, 32), new Vector2(0, 1)))
                    {
                        if (LocalChunk.CheckForPushCase(Position, new Vector2(0, 1)))
                        {
                            Sfx.PlaySound("drop", 0.8f);
                        }
                        else
                        {
                            LocalChunk.Move(Position, new Vector2(0, 1));

                            if (walkSoundCooldown == 0)
                            {
                                Sfx.PlaySound("walking2", 0.4f);
                                walkSoundCooldown = 12;
                            }
                        }

                        LockedMovementFrames = (int)Math.Ceiling(32f / MovementSpeed) + 1;
                    }
                }
                else if (keyboardState.IsKeyDown(Keys.Left))
                {
                    if (LocalChunk.TryToMove(Position + new Vector2(-32, 0), new Vector2(-1, 0)))
                    {
                        if (LocalChunk.CheckForPushCase(Position, new Vector2(-1, 0)))
                        {
                            Sfx.PlaySound("drop", 0.8f);
                        }
                        else
                        {
                            LocalChunk.Move(Position, new Vector2(-1, 0));

                            if (walkSoundCooldown == 0)
                            {
                                Sfx.PlaySound("walking2", 0.4f);
                                walkSoundCooldown = 12;
                            }
                        }

                        LockedMovementFrames = (int)Math.Ceiling(32f / MovementSpeed) + 1;
                    }
                }
                if (keyboardState.IsKeyDown(Keys.Z))
                {
                    if (Game.CommandManager.Count > 0)
                    {
                        LockedMovementFrames = (int)Math.Ceiling(32f / MovementSpeed) + 1;
                        Game.CommandManager.Undo();
                        Game.IsUndoing = true;
                    }
                }
            }

            if (walkSoundCooldown > 0)
            {
                walkSoundCooldown--;
            }

            ExecuteMove();

            if (!Game.IsEditing)
            {
                Game.MainChunk = LocalChunk;
            }

            if (LockedMovementFrames > 0)
            {
                LockedMovementFrames--;

                if (LockedMovementFrames == 0)
                {
                    endingTurn = true;
                }
            }

            previousKeyboardState = keyboardState;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Next != Position)
            {
                if (Next.X < Position.X)
                {
                    if (animationCount > 6)
                    {
                        u = 2;
                        v = 1;
                    }
                    else if (animationCount > 12)
                    {
                        u = 0;
                        v = 2;
                    }
                    else
                    {
                        u = 2;
                        v = 1;
                    }
                }
                else if (Next.X > Position.X)
                {
                    if (animationCount > 6)
                    {
                        u = 0;
                        v = 1;
                    }
                    else if (animationCount > 12)
                    {
                        u = 1;
                        v = 1;
                    }
                    else
                    {  
                        u = 0;
                        v = 1;
                    }
                }
                else if (Next.Y > Position.Y)
                {
                    if (animationCount > 6)
                    {
                        u = 0;
                        v = 0;
                    }
                    else if (animationCount > 12)
                    {
                        u = 1;
                        v = 0;
                    }
                    else
                    {
                        u = 2;
                        v = 0;
                    }
                }
                else if (Next.Y < Position.Y)
                {
                    if (animationCount > 6)
                    {
                        u = 1;
                        v = 2;
                    }
                    else if (animationCount > 12)
                    {
                        u = 2;
                        v = 2;
                    }
                    else
                    {
                        u = 0;
                        v = 3;
                    }
                }

                animationCount++;

                if (animationCount > 18)
                {
                    animationCount = 0;
                }
            }

            spriteBatch.Draw(texture, Position, new Rectangle(u * Tile.Size, v * Tile.Size, Tile.Size, Tile.Size), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
        }
    }
}
