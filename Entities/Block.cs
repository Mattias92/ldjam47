﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47.Entities
{
    [Serializable()]
    public class Block : Entity
    {
        public Block(Game1 game, ContentManager content, Vector2 position) : base(game)
        {
            Position = position;
            Next = position;
            texture = content.Load<Texture2D>("atlas12");
            texturePath = "atlas12";
            u = 1;
            v = 2;
            HasCollision = true;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            ExecuteMove();
        }
    }
}
