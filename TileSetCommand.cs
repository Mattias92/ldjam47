﻿using LDJam47.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam47
{
    public class TileSetCommand : ICommand
    {
        private Tile tile;
        private bool setHole;
        private int ou;
        private int ov;
        private int nu;
        private int nv;

        public TileSetCommand(Tile tile, int nu, int nv, bool setHole = false)
        {
            this.tile = tile;
            this.setHole = setHole;
            ou = tile.U;
            ov = tile.V;
            this.nu = nu;
            this.nv = nv;
        }

        public void Execute()
        {
            tile.U = nu;
            tile.V = nv;
            if (setHole)
                tile.HoleIsBlocked = !tile.HoleIsBlocked;
        }

        public void Undo()
        {
            tile.U = ou;
            tile.V = ov;
            if (setHole)
                tile.HoleIsBlocked = !tile.HoleIsBlocked;
        }
    }
}
